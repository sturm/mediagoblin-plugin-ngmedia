from mediagoblin.tools import pluginapi

def setup_plugin():
    pluginapi.register_commands({
        'sync': {
            'setup': 'ngmedia.sync:parser_setup',
            'func': 'ngmedia.sync:sync',
            'help': 'Do something'},
        })


def home_context(context):
    # This is used in our custom ngmedia theme.
    context['restricted'] = context['media'].media_metadata.get('NGM:Cultural access') == 'Restricted'
    context['published'] = context['media'].media_metadata.get('NGM:Published', '').lower() not in ['no', 'false', '']
    return context

# from mediagoblin.media_types.image import ImageMediaManager

# class RestrictedImageMediaManager(ImageMediaManager):
#     def __init__(self, *args, **kwargs):
#         self.entry = None

#     def __getitem__(self, i):
#         return None


# def manager(*args, **kwargs):
#     return RestrictedImageMediaManager

hooks = {
    'setup': setup_plugin,
    'media_home_context': home_context,
    # ('media_manager', 'mediagoblin.media_types.image'): manager,
}
