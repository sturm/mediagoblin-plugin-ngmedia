# GNU MediaGoblin -- federated, autonomous media hosting
# Copyright (C) 2011, 2012, 2019 MediaGoblin contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import os

from jsonschema.exceptions import ValidationError
from mediagoblin import mg_globals
from mediagoblin.db.models import LocalUser, MediaEntry
from mediagoblin.gmg_commands import util as commands_util
from mediagoblin.submit.lib import submit_media
from mediagoblin.tools.metadata import compact_and_validate
from mediagoblin.tools.translate import pass_to_ugettext as _

from .sheets import (
    get_google_service, get_data, data_as_dicts, apply_update, mark_error)

def parser_setup(subparser):
    subparser.description = """\
This command allows the administrator to upload many media files at once."""
    subparser.epilog = _("""For more information about how to properly run this
script (and how to format the metadata csv file), read the MediaGoblin
documentation page on command line uploading
<http://docs.mediagoblin.org/siteadmin/commandline-upload.html>""")
    subparser.add_argument(
        'username',
        help=_("Name of user these media entries belong to"))
    subparser.add_argument(
        '--celery',
        action='store_true',
        help=_("Don't process eagerly, pass off to celery"))


def upload_from_dir(app, user, service, spreadsheet_id, sheet, ingest_dir, done_subdir):
    done_path = os.path.join(ingest_dir, done_subdir)
    if not os.path.exists(done_path):
        os.makedirs(done_path)
    for filename in os.listdir(ingest_dir):
        if (
                os.path.isfile(os.path.join(ingest_dir, filename))
                and filename not in ['.stignore']
                and not filename.startswith('.syncthing')):
            print(filename)
            file_path = os.path.join(ingest_dir, filename)
            print(file_path)
            entry = submit_media(app, user, open(file_path, 'rb'), filename)
            os.rename(file_path, os.path.join(done_path, filename))
            service.spreadsheets().values().append(
                spreadsheetId=spreadsheet_id,
                range=sheet,
                valueInputOption='RAW',
                body={
                    'values': [
                        [
                            'Warning - Item lacks minimum required metadata',
                            datetime.date.today().strftime('%-d/%-m/%Y'),
                            '',  # ingest required
                            entry.slug,
                            os.path.join(done_subdir, filename),  # location
                        ]]
                }
            ).execute()


def sync(args):
    config = mg_globals.global_config.get('plugins', {}).get('ngmedia', {})
    ingest_dir = config.get('ingest_dir')
    done_subdir = config.get('done_subdir')
    spreadsheet_id = config.get('spreadsheet_id')
    sheet = config.get('sheet')

    # Run eagerly unless explicetly set not to
    if not args.celery:
        os.environ['CELERY_ALWAYS_EAGER'] = 'true'

    app = commands_util.setup_app(args)

    files_attempted = 0

    # get the user
    user = app.db.LocalUser.query.filter(
        LocalUser.username==args.username.lower()
    ).first()
    if user is None:
        print(_("Sorry, no user by username '{username}' exists".format(
                    username=args.username)))
        return

    service = get_google_service()
    data = get_data(service, spreadsheet_id, sheet)
    media_metadata = data_as_dicts(data)

    for index, file_metadata in enumerate(media_metadata):
        if not file_metadata.get('ingest required'):
            continue

        files_attempted += 1
        # In case the metadata was not uploaded initialize an empty dictionary.
        json_ld_metadata = compact_and_validate({})

        ### Pull the important media information for mediagoblin from the
        ### metadata, if it is provided.
        slug = file_metadata.get('slug')
        title = file_metadata.get('NGM:Title')
        description = file_metadata.get('NGM:Content summary')

        try:
            del file_metadata['NGM:Title']
        except KeyError:
            pass
        try:
            del file_metadata['NGM:Content summary']
        except KeyError:
            pass

        # Clear any empty metadata fields.
        for k, v in list(file_metadata.items()):
            if v == '':
                del file_metadata[k]

        # Clear the published field if not set, since we search the metadata
        # text for "Published" to determine if an item is published.
        if file_metadata.get('NGM:Published', '').lower() != 'yes':
            del file_metadata['NGM:Published']

        try:
            json_ld_metadata = compact_and_validate(file_metadata)
        except ValidationError as exc:
            media_id = file_metadata.get('id') or index
            error = _("""Error with media '{media_id}' value '{error_path}': {error_msg}
Metadata was not uploaded.""".format(
                media_id=media_id,
                error_path=exc.path[0],
                error_msg=exc.message))
            print(error)
            continue

        entry = MediaEntry.query.filter_by(actor=user.id, slug=slug).first()
        spreadsheet_index = index + 3
        if entry:
            entry.title = title or entry.title
            entry.description = description or entry.description
            entry.media_metadata = json_ld_metadata or entry.media_metadata
            entry.save()
            print(f'Updated entry {entry}.')
            apply_update(service, spreadsheet_id, sheet, spreadsheet_index, entry.title != '')
        else:
            print(f'Error updating entry {entry}.')
            mark_error(service, spreadsheet_id, sheet, spreadsheet_index)
        continue

    upload_from_dir(app, user, service, spreadsheet_id, sheet, ingest_dir, done_subdir)
