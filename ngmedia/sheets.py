# GNU MediaGoblin -- federated, autonomous media hosting
# Copyright (C) 2011, 2012, 2019 MediaGoblin contributors.  See AUTHORS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from collections import OrderedDict
import datetime
import os.path
import pickle

from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

def get_google_service():
    """Boilerplate to prepare authentication to Google Sheets API."""

    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    return build('sheets', 'v4', credentials=creds, cache_discovery=False)


def get_data(service, spreadsheet_id, range):
    """Fetch the full spreadsheet of data from the Google Sheet."""

    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=spreadsheet_id, range=range).execute()
    return result.get('values', [])


def data_as_dicts(data):
    """Convert data to dicts keyed by the header fields like csv.DictReader."""

    # Skip the first row.
    headers = data[1][:5] + ['NGM:' + header for header in data[1][5:]]
    for record in data[2:]:
        yield OrderedDict(zip(headers, record))


def apply_update(service, spreadsheet_id, sheet, row, complete=True):
    """Update the ingest status fields of a single record.

    We assume these are the first 3 fields of the spreadsheet.

    """
    # TODO: Check if we should import or update this record.
    # TODO: Do the import or update

    # Mark the record as imported/updated.
    range = f'{sheet}!A{row}:C{row}'
    # Setting the ingest status, date and overwriting the update required.
    if complete:
        values = [['OK - Ingest Complete', datetime.date.today().strftime('%-d/%-m/%Y'), '']]
    else:
        values = [['Warning - Item lacks minimum required metadata', datetime.date.today().strftime('%-d/%-m/%Y'), '']]
    body = {
        'values': values
    }
    return service.spreadsheets().values().update(
        spreadsheetId=spreadsheet_id, range=range,
        valueInputOption='RAW', body=body).execute()


def mark_error(service, spreadsheet_id, sheet, row):
    # Mark the record as imported/updated.
    range = f'{sheet}!A{row}:C{row}'
    # Setting the ingest status, date and overwriting the update required.
    values = [['Error - Ingest Failed. See Logs for more details', datetime.date.today().strftime('%-d/%-m/%Y'), '']]
    body = {
        'values': values
    }
    return service.spreadsheets().values().update(
        spreadsheetId=spreadsheet_id, range=range,
        valueInputOption='RAW', body=body).execute()
