NG Media
========

Provides a gmg command to upload any files from a local ingest direct to
MediaGoblin, writes their details to a Google Sheet and then updates metadata as
required from that Google Sheet.

This allows bulk editing via Google Sheets as well as GUI uploads via a custom
AppSheet app that modifies the Google Sheet.

Configure it like this:

[[ngmedia]]
ingest_dir = '/home/user/Sync'
spreadsheet_id = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
sheet = 'Sheet1'
