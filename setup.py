from setuptools import setup, find_packages

setup(
    name='ngmedia',
    version='1.0',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'google-api-python-client',
        'google-auth-httplib2',
        'google-auth-oauthlib',
    ],
    license='AGPLv3',
)
